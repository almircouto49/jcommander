package jCommander;

import com.beust.jcommander.Parameter;

public class HelloArgs {
	 @Parameter(names = {"--name", "-n"}, description = "User name",   required = true)
	 // ValidateWith = Validator.class
		    private String name;

	public String getName() {
		return name;
	}

}
